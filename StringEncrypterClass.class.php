<?php
// define constant
define('AES_256_CBC', 'aes-256-cbc');

// Required CSV files (final database)
$inputFile = "import.csv";
$outputFile = "export.csv";

class StringEncrypter {

	private $encryptionKey = "";
	private $initializationVector = false;


	public function __construct() {
		$this->initializationVector = openssl_random_pseudo_bytes(openssl_cipher_iv_length(AES_256_CBC));
	}


	/**
	 * Set new encrption key, if
	 * param is empty it it ignored.
	 */
	public function setEncryptionKey($key) {
		if($key) {
			$this->encryptionKey = $newKey;
		}
	}


	/**
	 * Encrypt string using encryption key
	 * param is string to encrypt
	 */
	public function encryptString($data) {
		$encrypted = false;

		if(isset($data)) {
			$encrypted = openssl_encrypt($data, AES_256_CBC, $this->encryptionKey, 0, $this->initializationVector);
			// If we lose the $iv variable, we can't decrypt this, so:
			// - $encrypted is already base64-encoded from openssl_encrypt
			// - Append a separator that we know won't exist in base64, ":"
			// - And then append a base64-encoded $iv
			$encrypted = $encrypted . ':' . base64_encode($this->initializationVector);
		}

		return $encrypted;
	}


	/**
	 * Decrypt string, string must contain 
	 * initilization vector after colon
	 */
	public function decryptString($string) {
		$decrypted = false;

		if($string) {
		// To decrypt, separate the encrypted data from the initialization vector ($iv).
			$parts = explode(':', $string);

			if(isset($parts[0]) && isset($parts[1])) {
				$decrypted = openssl_decrypt($parts[0], AES_256_CBC, $this->encryptionKey, 0, base64_decode($parts[1]));	
			}
		}

		return $decrypted;
	}


	/**
	 * Takes CSV structure of final database
	 * @param  [type] CSV of final database export
	 * @param  [type] CSV output location
	 */
	public function decryptCsv($inputFile, $outputFile) {

		if(!isset($inputFile) || !isset($outputFile)) {
			return false;
		}

		// empty output file
		touch($outputFile);

		$exportCount = 0;
		$file = fopen($inputFile,"r");
		$output = fopen($outputFile, 'w');

		// loop through CSV data
		while (($data = fgetcsv($file)) !== FALSE) {

			// push decoded string into CSV row
			array_push($data, $this->decryptString($data[2]));

			// add to output file
			$exportCount++;
			fputcsv($output, $data);
		}

		fclose($file);
		fclose($output);

		return $exportCount;
	}
}
?>
